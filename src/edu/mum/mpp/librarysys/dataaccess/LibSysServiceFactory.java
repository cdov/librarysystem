package edu.mum.mpp.librarysys.dataaccess;

import java.util.HashMap;

/**
 *
 * @author Huaijin Zhou
 *
 */
public class LibSysServiceFactory {
	private LibSysServiceFactory(){}
	static HashMap<Class, IBaseService> map = new HashMap<>();
	static {
		map.put(IPublication.class, new PublicationImpl());
		
		map.put(ILibraryMember.class, new LibraryMemberImpl());

	map.put(IAuthor.class, new AuthorImpl());
	}
	public static <T> T getBizService(Class classz) {
		if(!map.containsKey(classz)) {
			throw new IllegalArgumentException("No implementation found for this interface:" +classz.getSimpleName());
		}
		return (T) map.get(classz);
	}

	public static void main(String[] arg){
		IPublication aIPublication = LibSysServiceFactory.getBizService(IPublication.class);
        System.out.println("name:" + aIPublication.hashCode());
	}
}
