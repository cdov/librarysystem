package edu.mum.mpp.librarysys.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 */
public class User extends Person implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = -6154801760135748606L;
	/**
     * Default constructor
     */
    public User() {
    }

    /**
     * 
     */
    private String id;

    /**
     * 
     */
    private String password;
    private List<Role> roles=new ArrayList<Role>();
	public User(String firstName, String lastName, String phoneNumber, Address address, String id, String password,
			List<Role> roles) {
		super(firstName, lastName, phoneNumber, address);
		this.id = id;
		this.password = password;
		this.roles = roles;
	}
	public String getrequestId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public List<Role> getRoles() {
		return roles;
	}
	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}


}