package edu.mum.mpp.librarysys.dataaccess;

import edu.mum.mpp.librarysys.model.LibraryMember;


public interface ILibraryMember extends IBaseService<LibraryMember> {

//	public LibraryMember getLibraryMember(String name);

	public LibraryMember getLibraryMember(long memberID);
}
