package edu.mum.mpp.librarysys.dataaccess;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import edu.mum.mpp.librarysys.model.Author;

public class AuthorImpl  extends DataAccessBase implements IAuthor{

	@Override
	public Author getAuthor(String name) {
		List<Author> allAuthor = getAllItems();
		Optional<Author> foundAuthor = LambdaLibrary.getAuthorByName.apply(allAuthor,name);
		return foundAuthor.isPresent() ? foundAuthor.get() : null;
	}
	@Override
	public Author getAuthor(UUID id) {
		List<Author> allAuthor = getAllItems();
		Optional<Author> foundAuthor = LambdaLibrary.getAuthorById.apply(allAuthor,id);
		return foundAuthor.isPresent() ? foundAuthor.get() : null;
	}


	@Override
	public void add(Author obj) {
		// TODO Auto-generated method stub
		List<Author> allAuthor = getAllItems();
		allAuthor.add(obj);
		save(allAuthor);
	}

	@Override
	public void delete(int index) {
		// TODO Auto-generated method stub
		List<Author> allAuthor = getAllItems();
		allAuthor.remove(index);
		save(allAuthor);
	}

	@Override
	public void edit(Author editObj) {
		// TODO Auto-generated method stub
		List<Author> allAuthor = getAllItems();

		for (int i = 0; i < allAuthor.size(); i++) {
			if (allAuthor.get(i).getId().equals(editObj.getId())) {
				allAuthor.remove(i);
				allAuthor.add(i, editObj);
			}
		}

		save(allAuthor);
	}

	@Override
	public List<Author> listAll() {
		// TODO Auto-generated method stub
		List<Author> allAuthors = getAllItems();
		return allAuthors;
	}

}
