package edu.mum.mpp.librarysys.dataaccess;

import java.util.List;
import java.util.Optional;

import edu.mum.mpp.librarysys.model.LibraryMember;

/**
*
* @author Chi Proeng DOV
*
*/
public class LibraryMemberImpl extends DataAccessBase implements ILibraryMember {
	

//	@Override
//	public LibraryMember getLibraryMember(String name) {
//		// TODO Auto-generated method stub
//		ArrayList<LibraryMember> allMembers = getAllItems();
//		Optional<LibraryMember> libraryMember=allMembers.stream().filter(member->member).findFirst();
//		return null;
//	}
	

	@Override
	public LibraryMember getLibraryMember(long memberID) {
		// TODO Auto-generated method stub
		List<LibraryMember> allMembers = getAllItems();
		Optional<LibraryMember> libraryMember=allMembers.stream().filter(member->member.getMemberId()==memberID).findFirst();
		return libraryMember.isPresent()?libraryMember.get():null;
	}

	@Override
	public void add(LibraryMember obj) {
		// TODO Auto-generated method stub
		List<LibraryMember> allMembers = getAllItems();
		allMembers.add(obj);
		save(allMembers);
	}

	@Override
	public void delete(int index) {
		// TODO Auto-generated method stub
		List<LibraryMember> allMembers = getAllItems();
		allMembers.remove(index);
		save(allMembers);
	}

	@Override
	public void edit(LibraryMember editObj) {
		// TODO Auto-generated method stub
		List<LibraryMember> allMembers = getAllItems();
//				for (LibraryMember libraryMember : allMembers) {
//					if(libraryMember.getMemberId()==currentMember.getMemberId()){
//						libraryMember=currentMember;
//						super.save(allMembers);
//						break;
//					}
//				}
		for (int i = 0; i < allMembers.size(); i++) {
			if (allMembers.get(i).getMemberId()==editObj.getMemberId()) {
				allMembers.remove(i);
				allMembers.add(i, editObj);
				super.save(allMembers);
				break;
			}
		}

	}

	@Override
	public List<LibraryMember> listAll() {
		// TODO Auto-generated method stub
		return getAllItems();
	}
	
}
