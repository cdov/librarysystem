package edu.mum.mpp.librarysys.dataaccess;


import edu.mum.mpp.librarysys.model.Copy;
import edu.mum.mpp.librarysys.model.Publication;
import edu.mum.mpp.librarysys.util.AppException;

/**
 *
 * @author Huaijin Zhou
 *
 */
public interface IPublication extends IBaseService<Publication> {
	public void addACopy2Book(String isbn, Copy copy) throws AppException;

	public void addABook(Publication publication, int copyNum) throws AppException;

	public Copy checkoutACopyByISBN(String isbn) throws AppException;

	public Publication getByISBN(String isbn);
	public void updateAvailabilityCopy(String isbn,Copy copy,boolean bool) throws AppException ;
	public void addNewCopies2Book(String isbn, int copyNumber) throws AppException;
}
