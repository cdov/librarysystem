package edu.mum.mpp.librarysys.model;

import java.io.Serializable;
import java.util.UUID;

/**
 * 
 */
public class LibraryMember extends Person implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5082986038391910671L;
	private long memberId;
	private CheckoutRecord checkoutRecord;
	
    /**
     * Default constructor
     */
    public LibraryMember() {
    	id = UUID.randomUUID(); 
    }
    
    // 
    public void setId(UUID id) {
		this.id = id;
	}

    /**
     * 
     */    
	
	public LibraryMember(String firstName, String lastName, String phoneNumber, Address address
			,long memberId, CheckoutRecord checkoutRecord) {
		super(firstName, lastName, phoneNumber, address);
		// TODO Auto-generated constructor stub
		this.memberId = memberId;
		this.checkoutRecord = checkoutRecord;
		id = UUID.randomUUID(); 
	}

	public long getMemberId() {
		return memberId;
	}

	public void setMemberId(long memberId) {
		this.memberId = memberId;
	}

	public CheckoutRecord getCheckoutRecord() {
		return checkoutRecord;
	}

	public void setCheckoutRecord(CheckoutRecord checkoutRecord) {
		this.checkoutRecord = checkoutRecord;
	}


}