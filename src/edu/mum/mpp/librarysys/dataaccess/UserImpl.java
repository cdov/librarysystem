package edu.mum.mpp.librarysys.dataaccess;


import java.util.List;
import java.util.Optional;

import edu.mum.mpp.librarysys.model.User;

public class UserImpl extends DataAccessBase implements UserIn {

	@Override
	public void deleteUser(int index)
	{
		List<User> allUser = getAllItems();
		allUser.remove(index);
		save(allUser);
        	}
	
	@Override
	public void editUser(User currentUser) {
		List<User> allUser = getAllItems();

		for (int i = 0; i < allUser.size(); i++) {
			if (allUser.get(i).getId().equals(currentUser.getId())) {
				allUser.remove(i);
				allUser.add(i, currentUser);
			}
		}

		save(allUser);
	}
	// get user by ID 

	@Override
	public User getUser(String requestId) {
		List<User> allUser = getAllItems();
		Optional<User> foundUser = LambdaLibrary.getUserByRequestId.apply(allUser,requestId);
		return foundUser.isPresent() ? foundUser.get() : null;
		
	}

	public void addUser(User user) {
		List<User> allUser = getAllItems();
		allUser.add(user);
		save(allUser);
	}
	
	
}




