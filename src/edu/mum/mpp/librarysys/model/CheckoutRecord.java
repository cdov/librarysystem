package edu.mum.mpp.librarysys.model;

import java.io.Serializable;
import java.util.*;

/**
 * 
 */
public class CheckoutRecord implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = -7361300160588596848L;

	/**
     * Default constructor
     */
    public CheckoutRecord() {
    }

    /**
     * 
     */
    private Date paidDate;

    /**
     * 
     */
    private double fine;
    
    private LibraryMember libraryMember=new LibraryMember();
    private List<CheckoutRecordEntry> checkoutRecordEntries=new ArrayList<CheckoutRecordEntry>();
    
	public CheckoutRecord(Date paidDate, double fine, LibraryMember libraryMember) {
		this.paidDate = paidDate;
		this.fine = fine;
		this.libraryMember = libraryMember;
	}
	
	public Date getPaidDate() {
		return paidDate;
	}
	public void setPaidDate(Date paidDate) {
		this.paidDate = paidDate;
	}
	public double getFine() {
		return fine;
	}
	public void setFine(double fine) {
		this.fine = fine;
	}
	public LibraryMember getLibraryMember() {
		return libraryMember;
	}
	public void setLibraryMember(LibraryMember libraryMember) {
		this.libraryMember = libraryMember;
	}

	public List<CheckoutRecordEntry> getCheckoutRecordEntries() {
		return checkoutRecordEntries;
	}

	public void setCheckoutRecordEntries(List<CheckoutRecordEntry> checkoutRecordEntries) {
		this.checkoutRecordEntries = checkoutRecordEntries;
	}
	
	public void addEntry(CheckoutRecordEntry checkoutRecordEntry){		
		checkoutRecordEntries.add(checkoutRecordEntry);
	}


}