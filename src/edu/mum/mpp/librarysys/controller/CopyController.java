package edu.mum.mpp.librarysys.controller;


import java.util.List;

import edu.mum.mpp.librarysys.config.config2;
import edu.mum.mpp.librarysys.dataaccess.IPublication;
import edu.mum.mpp.librarysys.dataaccess.LibSysServiceFactory;
import edu.mum.mpp.librarysys.dataaccess.LibraryMemberImpl;
import edu.mum.mpp.librarysys.dataaccess.PublicationImpl;
import edu.mum.mpp.librarysys.model.Author;
import edu.mum.mpp.librarysys.model.Book;
import edu.mum.mpp.librarysys.model.Copy;
import edu.mum.mpp.librarysys.model.LibraryMember;
import edu.mum.mpp.librarysys.model.Publication;
import edu.mum.mpp.librarysys.util.AppException;
import edu.mum.mpp.librarysys.util.CommonUtil;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

public class CopyController {
	  	@FXML
	    private TextField txtMemberID;

	    @FXML
	    private Label lblFullName;

	    @FXML
	    private Label lblISBN;

	    @FXML
	    private TableView<Copy> TVBookCopy;

	    @FXML
	    private TableColumn<Copy, String> copyId;

	    @FXML
	    private TableColumn<Copy, String> availability;

	    @FXML
	    private Button btnMemberSearch;

	    @FXML
	    private Label lblMemberID;

	    @FXML
	    private Button btnBookSearch;

	    @FXML
	    private TextField txtISBN;

	    @FXML
	    private TextField txtTheNewCopyNumber;

	    @FXML
	    private Label lblTitle;

	    @FXML
	    private Label lblCheckoutLength;


	    @FXML
	    private Label lblAuthors;


	    @FXML
	    private void initialize() {
	        // Initialize the person table with the two columns.
	    	copyId.setCellValueFactory(cellData -> cellData.getValue().getCopyNoProperty());
	    	availability.setCellValueFactory(cellData -> cellData.getValue().getIsAvailableProperty());

	    }



	    @FXML
	    private void searchBook(){
	        IPublication iPublication = LibSysServiceFactory.getBizService(IPublication.class);

//	    	PublicationImpl publicationImpl = new PublicationImpl();
	    	Publication pub=iPublication.getByISBN(txtISBN.getText());
	    	if(pub==null){
	    		Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("No Publication Found");
				alert.setHeaderText(null);
				alert.setContentText("No Publication Found, please retype!");
				alert.showAndWait();
				txtISBN.setText("");
				txtISBN.requestFocus();
	    	}
	    	lblISBN.setText(pub.getIsbn());
	    	lblTitle.setText(pub.getTitle());
	    	lblCheckoutLength.setText(pub.getCheckoutLength()+" Days");
	    	if(pub instanceof Book){
	    	   Book aBook = (Book) pub;
	    	   String authors ="";
	    	   if(aBook.getAuthors()!=null){
	    		   List<Author> autList =aBook.getAuthors();
	    		   int i =0;
	    		   for(Author aAuthor : autList){
	    			   if(i>0){
	    				   authors =authors+"/";
	    			   }
	    			   authors = authors +aAuthor.getLastName()+"," + aAuthor.getFirstName();
	    			   i++;
	    		   }
	    		   lblAuthors.setText(authors);
	    	   }

	    	}
	    	//txtISBN.setText("");
	    	TVBookCopy.setItems(FXCollections.observableArrayList(pub.getCopies()));

	    }

	    @FXML
	    public void handleMakeNewCopy(){
	    	String num = txtTheNewCopyNumber.getText();
	        if (CommonUtil.isBlank(num)) {
	            config2.dialog(Alert.AlertType.ERROR, "Copy number should Not Empty");
	            txtTheNewCopyNumber.requestFocus();
	            return;
	        }
	        IPublication iPublication = LibSysServiceFactory.getBizService(IPublication.class);
//	        PublicationImpl publicationImpl = new PublicationImpl();

	        try {
	        	iPublication.addNewCopies2Book(txtISBN.getText(), Integer.parseInt(num));
				updateTableView(txtISBN.getText());
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (AppException e) {
				e.printStackTrace();
				config2.dialog(Alert.AlertType.ERROR, "Failed to make new copies for the book. ISBN:"+txtISBN.getText() );
			}
	    }
	    private void updateTableView(String isbn){

	    	PublicationImpl publicationImpl = new PublicationImpl();
	    	Publication pub=publicationImpl.getByISBN(isbn);
	    	TVBookCopy.setItems(FXCollections.observableArrayList(pub.getCopies()));
	    }
}
