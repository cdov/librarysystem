package edu.mum.mpp.librarysys.model;

import java.io.Serializable;
import java.util.*;

/**
 *
 */
public class Publication implements Serializable{

    /**
	 *
	 */
	private static final long serialVersionUID = -4714742154034104356L;

	/**
     * Default constructor
     */
	private UUID id;
    public Publication() {
    	this.id = UUID.randomUUID();

    }
    public UUID getId() {
		return id;
	}

    /**
     *
     */
    private String title;

    /**
     *
     */
    private int checkoutLength;

    private transient String authorStr="";

    private transient String copyInfoStr="";
    /**
     *
     */
    private int copyAvailable;
    private String isbn;
    private List<Copy> copies=new ArrayList<Copy>();

	public Publication(String title, int checkoutLength,String isbn, int copyAvailable, List<Copy> copies) {
		this.title = title;
		this.checkoutLength = checkoutLength;
		this.copyAvailable = copyAvailable;
		this.copies = copies;
		this.isbn=isbn;
		this.id = UUID.randomUUID();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getCheckoutLength() {
		return checkoutLength;
	}

	public void setCheckoutLength(int checkoutLength) {
		this.checkoutLength = checkoutLength;
	}

	public int getCopyAvailable() {
		return copyAvailable;
	}

	public void setCopyAvailable(int copyAvailable) {
		this.copyAvailable = copyAvailable;
	}

	public List<Copy> getCopies() {
		return copies;
	}

	public void setCopies(List<Copy> copies) {
		this.copies = copies;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	//transient value
	public String getAuthorStr() {
 	   String authorStr ="";
		if(this instanceof Book){
			Book aBook = (Book) this;

    	   if(aBook.getAuthors()!=null){
   			   int i=0;
    		   List<Author> autList =aBook.getAuthors();
    		   for(Author aAuthor : autList){
    			   if(i>0){
    				   authorStr =authorStr+"/";
    			   }
    			   authorStr = authorStr +aAuthor.getLastName()+", " + aAuthor.getFirstName();
    			   i++;
    		   }
    	   }
		}
 	    this.authorStr=authorStr;
		return this.authorStr;
	}

	//transient value
	public String getCopyInfoStr() {
 	   //String rtnStr ="";
 	   List<Copy> copies = this.getCopies();
 	   if(copies==null || copies.size()==0){
 		   return "0|0";
 	   }
 	   int avaNum =0;
	   for(Copy aCopy : copies){
		   if(aCopy.isAvailability()){
			   avaNum++;
		   }
	   }
	   copyInfoStr = avaNum+"|"+copies.size();
	   return copyInfoStr;
	}
}