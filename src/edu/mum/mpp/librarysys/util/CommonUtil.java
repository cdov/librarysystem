package edu.mum.mpp.librarysys.util;

import java.util.List;


public class CommonUtil {
   public static boolean isBlank(String value){
	   if(value==null || value.trim().equals("")){
		  return true;
	   }
	   return false;
   }
   public static boolean isNotBlank(String value){

	   return !isBlank(value);
   }

   public static boolean isEmpty(String value){
	   if(value==null || value.equals("")){
		  return true;
	   }
	   return false;
   }

   public static boolean isNotEmpty(String value){
	   return !isEmpty(value);
   }

   public static int parseInt(String value){
	   if(isEmpty(value)){
		   return 0;
	   }
	   return Integer.parseInt(value.trim());
   }

   public static String convertListElementsToStringSeparatedWithComma(List<String> dataList){
	   if(dataList==null || dataList.size()==0){
		   return "";
	   }
	   int i=0;
	   String rtnStr="";
	   for(String el : dataList){
		   if(i>0){
			   rtnStr = rtnStr+",";
		   }
		   rtnStr= rtnStr+el;
		   i++;
	   }
	   return rtnStr;
   }
	public static void numericRule(String fieldName, String fieldValue, boolean isEmpty) throws AppException {
		if(!isEmpty){
			if(isEmpty(fieldValue)){
				throw new AppException(fieldName + " must be nonempty.");
			} else{
				return;
			}
		}
		try {
			Integer.parseInt(fieldValue);
		} catch(NumberFormatException e) {
			throw new AppException(fieldName + " must be numeric.");
		}
	}
	public static void numericRule(String fieldName, String fieldValue) throws AppException {
		numericRule(fieldName,fieldValue,false);
	}
	public static void numericWithDigitsLimitationRule(String fieldName, String fieldValue,int digits ,boolean isEmptyAllowed) throws AppException {
		if(isEmpty(fieldValue)){
			if(!isEmptyAllowed){
				throw new AppException(fieldName + " must be nonempty.");
			}
			return;
		}
		try {
			Integer.parseInt(fieldValue);
		} catch(NumberFormatException e) {
			throw new AppException(fieldName + " must be numeric.");
		}
	    if(fieldValue.trim().length()!=digits){
	    	throw new AppException(fieldName + " must contain "+digits +" digits.");
	    }
	}
	public static void numericWithDigitsLimitationRule(String fieldName, String fieldValue,int digits) throws AppException {
		CommonUtil.numericWithDigitsLimitationRule(fieldName, fieldValue, digits, false);
	}
	public static void stringWithDigitsAndRangeLimitationRule(String fieldName, String fieldValue,int digits,char minValue, char maxValue, boolean isEmptyAllowed) throws AppException {
		if(isEmpty(fieldValue)){
			if(!isEmptyAllowed){
				throw new AppException(fieldName + " must be nonempty.");
			}
			return;
		}
	    if(fieldValue.trim().length()!=digits){
	    	throw new AppException(fieldName + " must contain "+digits +" digits.");
	    }
	    char[] chars = fieldValue.toCharArray();
	    for(char ch : chars){
	    	if(ch<minValue || ch >maxValue){
	    		throw new AppException(fieldName + " must be between "+minValue +" and " +maxValue +".");
	    	}
	    }
	}
	public static void stringWithDigitsAndRangeLimitationRule(String fieldName, String fieldValue,int digits,char minValue, char maxValue) throws AppException {
		CommonUtil.stringWithDigitsAndRangeLimitationRule(fieldName, fieldValue, digits,minValue,maxValue, false);
	}

	public static void checkTheCharRangeOfTheString(String fieldName, String fieldValue,char minValue, char maxValue, String errorMsg) throws AppException {
		if(isEmpty(fieldValue)){
			return;
		}
        if(errorMsg==null){
        	errorMsg = fieldName +" must be between "+minValue +" and " +maxValue +".";
        }
	    char[] chars = fieldValue.toCharArray();
	    for(char ch : chars){
	    	if(ch<minValue || ch >maxValue){
	    		throw new AppException(errorMsg);
	    	}
	    }
	}
}
