package edu.mum.mpp.librarysys.model;

import java.io.Serializable;
import java.util.UUID;

/**
 *
 */
public class Author extends Person implements Serializable{

    /**
	 *
	 */
	private static final long serialVersionUID = -813733043966829501L;

	public String getCredential() {
		return credential;
	}

	public void setCredential(String credential) {
		this.credential = credential;
	}

	public String getShorBio() {
		return shorBio;
	}

	public void setShorBio(String shorBio) {
		this.shorBio = shorBio;
	}

	public Author(String credential, String shorBio) {
		this.credential = credential;
		this.shorBio = shorBio;
	}

	/**
     * Default constructor
     */
	public void setId(UUID id) {
		this.id = id;
	}
    public Author() {
    	id = UUID.randomUUID();

    }

    /**
     *
     */
    private String credential;

    private transient String zip;

    /**
     *
     */
    private String shorBio;

	public Author(String firstName, String lastName, String phoneNumber, Address address, String credential,
			String shorBio) {
		super(firstName, lastName, phoneNumber, address);
		this.credential = credential;
		this.shorBio = shorBio;
	}

	@Override
	public String toString() {
		return this.getFirstName()+this.getLastName();
	}

	public String getZip(){
		if(super.getAddress()!=null){
			zip = super.getAddress().getZip();;
		}
		return zip;
	}


}