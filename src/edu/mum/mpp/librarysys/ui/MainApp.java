package edu.mum.mpp.librarysys.ui;
import java.io.IOException;

import edu.mum.mpp.librarysys.controller.HomeController;
import edu.mum.mpp.librarysys.util.ScreenIndex;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class MainApp extends Application {
	  private Stage primaryStage;
	  private BorderPane rootLayout;
	  @Override
	    public void start(Stage primaryStage) {
	        this.primaryStage = primaryStage;
	        this.primaryStage.setTitle("Library System");

	        initRootLayout();
	        viewLoginPage();
	    }

	  public void initRootLayout() {
	        try {
	                rootLayout = (BorderPane) FXMLLoader.load(MainApp.class.getResource("../view/Layout.fxml"));

	               // Show the scene containing the root layout.
	               Scene scene = new Scene(rootLayout);
	               primaryStage.setScene(scene);
	               primaryStage.show();
	           }
	        catch (IOException e) {
	               e.printStackTrace();
	           }
	        }
    public void viewLoginPage() {
        try {
        	 FXMLLoader loader = new FXMLLoader();
             loader.setLocation(MainApp.class.getResource(ScreenIndex.mainScreenFile));
             AnchorPane loginOverview = (AnchorPane) loader.load();
             rootLayout.setCenter(loginOverview);
         } catch (IOException e) {
             e.printStackTrace();
         }
    }


    public void viewHomeMenu()
    {
    	 try {
    		 Parent root = FXMLLoader.load(getClass().getResource(ScreenIndex.mainFile));
    	        Scene scene = new Scene(root);
    	        primaryStage.setScene(scene);
    	        primaryStage.initStyle(StageStyle.UNDECORATED);
    	        primaryStage.show();
    		 //rootLayout = (BorderPane) FXMLLoader.load(MainApp.class.getResource(ScreenIndex.mainFile));
    		 // Scene scene = new Scene(rootLayout);
               primaryStage.setScene(scene);
               primaryStage.show();
        	 FXMLLoader loader = new FXMLLoader();
             loader.setLocation(MainApp.class.getResource(ScreenIndex.mainFile));
           //  AnchorPane mainOverview = (AnchorPane) loader.load();
          //   rootLayout.setCenter(mainOverview);

             // Give the controller access to the main app.
             HomeController controller = loader.getController();
             controller.setMainApp(this);

         } catch (IOException e) {
             e.printStackTrace();
         }
    }

    public static void main(String[] args) {
        launch(args);
    }
    //
}


