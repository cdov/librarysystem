package edu.mum.mpp.librarysys.controller;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.mum.mpp.librarysys.animation.FadeInUpTransition;
import edu.mum.mpp.librarysys.config.config2;
import edu.mum.mpp.librarysys.dataaccess.IAuthor;
import edu.mum.mpp.librarysys.dataaccess.IPublication;
import edu.mum.mpp.librarysys.dataaccess.LibSysServiceFactory;
import edu.mum.mpp.librarysys.model.Author;
import edu.mum.mpp.librarysys.model.Book;
import edu.mum.mpp.librarysys.model.Publication;
import edu.mum.mpp.librarysys.util.AppException;
import edu.mum.mpp.librarysys.util.CommonUtil;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class BookController implements BaseViewController {
	ScreenController myController;

	protected final String PAGE_TITLE_ADD="Add New Book";
	protected final String PAGE_TITLE_EDIT="Edit Book";
	@FXML
	private TableView<Author> tableAuthorList;


    @FXML
    private TableView<Publication> booksview;

    private Map<String, Author> selectedAuthorMap = new HashMap<String, Author>();
	@FXML
	private TableColumn<Author, String> colFirstName;// found
	@FXML
	private TableColumn<Author, String> colZip;// found
	@FXML
	private TableColumn<Author, String> colLastName;// found
    @FXML
    private TextField txtTitle;       //found
    @FXML
    private TextField txtIsbn;// found
    @FXML
    private TextField txtCheckoutDays;// found
    @FXML
    private TextField txtCopyNumber;// found

    @FXML
    private TextField txtSelectedAuthors;// found
    @FXML
    private AnchorPane paneCrud;        //found
    @FXML
    private AnchorPane paneTabel;       //found
    @FXML
    private Button btnNewBook;

    private ObservableList<Author> listData;   // list of Author
    @FXML
    private Button btnSave;  //found

	@FXML
	private TableColumn<Publication, String> title;
	@FXML
	private TableColumn<Publication, String> isbn;
	@FXML
	private TableColumn<Publication, String> auther;

	@FXML
	private TableColumn<Publication, String> maxCheckoutLength;
	@FXML
	private TableColumn<Publication, String> copyInfoStr;


	@FXML
    private Label lblPageTitle;
    Stage stage;


	public void setScreenParent(ScreenController screenParent) {
		myController = screenParent;
	}

	@FXML
	private void handlePreAddNewBook(ActionEvent event) throws AppException {

		IAuthor iAuthor = LibSysServiceFactory.getBizService(IAuthor.class);
		List<Author> authorListData = iAuthor.listAll();
		colFirstName.setCellValueFactory(new PropertyValueFactory<Author, String>("firstName"));

		colLastName.setCellValueFactory(new PropertyValueFactory<Author, String>("lastName"));

		colZip.setCellValueFactory(new PropertyValueFactory<Author, String>("zip"));

		tableAuthorList.setItems(FXCollections.observableArrayList(authorListData));
		paneCrud.setOpacity(0);
		// this.clear();
		txtCopyNumber.setEditable(true);
		btnNewBook.setVisible(false);
		lblPageTitle.setText(PAGE_TITLE_ADD);
		new FadeInUpTransition(paneCrud).play();
		Platform.runLater(() -> {
			clear();

			// auto();
		});
	}

	@FXML
	private void handlePreEditNewBook() throws AppException {

		IAuthor iAuthor = LibSysServiceFactory.getBizService(IAuthor.class);
		List<Author> authorListData = iAuthor.listAll();
		colFirstName.setCellValueFactory(new PropertyValueFactory<Author, String>("firstName"));

		colLastName.setCellValueFactory(new PropertyValueFactory<Author, String>("lastName"));

		colZip.setCellValueFactory(new PropertyValueFactory<Author, String>("zip"));
		tableAuthorList.setItems(FXCollections.observableArrayList(authorListData));
		// prepare data for edit book
		this.clear();
		Publication aPublication = booksview.getSelectionModel().getSelectedItem();
		txtTitle.setText(aPublication.getTitle());
		txtIsbn.setText(aPublication.getIsbn());
		txtCheckoutDays.setText(String.valueOf(aPublication.getCheckoutLength()));
		int copyNum = (aPublication.getCopies() != null) ? aPublication.getCopies().size() : 0;
		txtCopyNumber.setText(String.valueOf(copyNum));
		txtCopyNumber.setEditable(false);
        //handle the author info, retrieve the data and save author objects into a map
		txtSelectedAuthors.setText(retrieveAuthorInfoFromABook(aPublication));

		paneTabel.setOpacity(0);
		btnNewBook.setVisible(false);
		lblPageTitle.setText(PAGE_TITLE_EDIT);
		new FadeInUpTransition(paneCrud).play();
		Platform.runLater(() -> {

			// auto();
		});
	}
	private String retrieveAuthorInfoFromABook(Publication aPublication){
		if(!(aPublication instanceof Book)){
			return "";
		}
		Book aBook = (Book) aPublication;
		if(aBook.getAuthors()==null){
			return "";
		}
		List<Author> authorList = aBook.getAuthors();
		String rtnStr=" ";
		for(Author aAuthor : authorList){
			rtnStr = rtnStr +aAuthor.toString()+" ";
			selectedAuthorMap.put(aAuthor.toString(), aAuthor);
		}
		return rtnStr;
	}
	@FXML
	private void initialize() throws AppException {

		// get books and fill them in the tables


		IPublication iPublication = LibSysServiceFactory.getBizService(IPublication.class);
		List<Publication> publication;
			publication = iPublication.listAll();
		title.setCellValueFactory(new PropertyValueFactory<Publication, String>(
				"title"));

		isbn.setCellValueFactory(new PropertyValueFactory<Publication, String>("isbn"));
		maxCheckoutLength.setCellValueFactory(new PropertyValueFactory<Publication, String>("checkoutLength"));

		auther.setCellValueFactory(new PropertyValueFactory<Publication, String>("authorStr"));

		copyInfoStr.setCellValueFactory(new PropertyValueFactory<Publication, String>("copyInfoStr"));


		booksview.setItems(FXCollections.observableArrayList(publication));
     paneCrud.setOpacity(0);
     new FadeInUpTransition(paneTabel).play();
     }


	@FXML
    private void handleClickAuthor() {
		Author aAuthor = tableAuthorList.getSelectionModel().getSelectedItem();
		String selectedValues = txtSelectedAuthors.getText();
		if(selectedValues==null || "".equals(selectedValues)){
			selectedValues=" ";
		}
		if(selectedValues.indexOf(" " + aAuthor.toString() +" ")>-1){
			selectedValues =selectedValues.replaceAll(" "+aAuthor.toString(), "");
			selectedAuthorMap.remove(aAuthor.toString());
		} else{
		    selectedValues =selectedValues + aAuthor.toString() +" ";
		    selectedAuthorMap.put(aAuthor.toString(), aAuthor);
		}
		txtSelectedAuthors.setText(selectedValues);
	}

	/**
	 * This method handles the requests regarding 'add new book' and 'edit book'
	 * @param event
	 */
	@FXML
    private void handleSaveBook(ActionEvent event) {

    	 config2 config = new config2();
        if (CommonUtil.isBlank(txtTitle.getText())) {
            config2.dialog(Alert.AlertType.ERROR, "Title should Not Empty");
            txtTitle.requestFocus();
            return;
        }
        if (CommonUtil.isBlank(txtIsbn.getText())) {
            config2.dialog(Alert.AlertType.ERROR, "ISBN should Not Empty");
            txtIsbn.requestFocus();
            return;
        }
        try{
        	CommonUtil.numericRule("CheckoutDays", txtCheckoutDays.getText());
        } catch(AppException e){
        	config2.dialog(Alert.AlertType.ERROR, "CheckoutDays should not be empty and should be a numberic value.");
        	txtCopyNumber.requestFocus();
        	return;
        }
        try{
        	CommonUtil.numericRule("Copy number", txtCopyNumber.getText());
        } catch(AppException e){
        	config2.dialog(Alert.AlertType.ERROR, "Copy number should not be empty and should be a numberic value.");
        	txtCopyNumber.requestFocus();
        	return;
        }

        if (CommonUtil.isBlank(txtSelectedAuthors.getText())) {
            config2.dialog(Alert.AlertType.ERROR, "Author should Not Empty");
            //txtSelectedAuthors.requestFocus();
            return;
        }
        int copyNumber = Integer.parseInt(txtCopyNumber.getText());
        IPublication iPublication = LibSysServiceFactory.getBizService(IPublication.class);
        //prepare author information
        Collection<Author> selectedAuthorCol = selectedAuthorMap.values();
        List selectedAuthorList = Arrays.asList(selectedAuthorCol.toArray());
        Publication publication = new Book(txtTitle.getText(),Integer.parseInt(txtCheckoutDays.getText()),txtIsbn.getText(),
        		copyNumber,null,"1", selectedAuthorList);
        try {
        	String pageTitle = lblPageTitle.getText();
        	if(PAGE_TITLE_EDIT.equals(pageTitle)){
			    iPublication.edit(publication);
        	} else{
        		iPublication.addABook(publication, copyNumber);
        	}
		} catch (AppException e) {
			config2.dialog(Alert.AlertType.ERROR, "Failed to save this Book. Error Message:" +e.getErrorMsg());
			return;
		}
        config2.dialog(Alert.AlertType.INFORMATION, "Succeeded to save this Book.");
    }



    private void clear(){
//        txtCity.clear();
//        txtZip.clear();
//        txtLastName.clear();
//        txtStreet.clear();
//        txtFirstName.clear();
//        txtPhone.clear();
//        txtState.clear();
    	txtTitle.clear();
    	txtIsbn.clear();
    	txtCheckoutDays.clear();
    	txtCopyNumber.clear();
    	txtSelectedAuthors.clear();
    	selectedAuthorMap.clear();
    }

    @FXML
    private void handleBack(ActionEvent event) throws AppException {
    	 IPublication iPublication = LibSysServiceFactory.getBizService(IPublication.class);
			List<Publication> publication;
			publication = iPublication.listAll();
			booksview.setItems(FXCollections.observableArrayList(publication));
			 paneCrud.setOpacity(0);
			 btnNewBook.setVisible(true);
			new FadeInUpTransition(paneTabel).play();
        Platform.runLater(() -> {
        	clear();


     // auto();
     });
    }
}
