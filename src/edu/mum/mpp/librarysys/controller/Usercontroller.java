package edu.mum.mpp.librarysys.controller;

import java.net.URL;
import java.util.ResourceBundle;

import edu.mum.mpp.librarysys.animation.FadeInLeftTransition;
import edu.mum.mpp.librarysys.animation.FadeInLeftTransition1;
import edu.mum.mpp.librarysys.animation.FadeInRightTransition;
import edu.mum.mpp.librarysys.config.config2;
import edu.mum.mpp.librarysys.dataaccess.UserImpl;
import edu.mum.mpp.librarysys.model.Role;
import edu.mum.mpp.librarysys.model.User;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class Usercontroller implements Initializable {
	static User user;
	Role role;
    @FXML
    private TextField txtRequestID;
    @FXML
    private PasswordField txtPassword;
    @FXML
    private Text lblWelcome;
    @FXML
    private Text lblUserLogin;
    @FXML
    private Text lblUsername;
    @FXML
    private Text lblPassword;
    @FXML
    private Button btnLogin;
    @FXML
    private Text lblRudyCom;
    @FXML
    private Label lblClose;
    Stage stage;
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Platform.runLater(() -> {
        		      
    	        
            new FadeInRightTransition(lblUserLogin).play();
            new FadeInLeftTransition(lblWelcome).play();
            new FadeInLeftTransition1(lblPassword).play();
            new FadeInLeftTransition1(lblUsername).play();
            new FadeInLeftTransition1(txtRequestID).play();
            new FadeInLeftTransition1(txtPassword).play();
            new FadeInRightTransition(btnLogin).play();
            lblClose.setOnMouseClicked((MouseEvent event) -> {
                Platform.exit();
                System.exit(0);
            });
        });
        // TODO
    }

    @FXML
    private void aksiLogin(ActionEvent event) {
    	// Get the Users and Compare them with txtUser & txtPassword if Ok then forword to HomeView
    	UserImpl userImpl=new UserImpl();
     user=userImpl.getUser(txtRequestID.getText());
    	if(user!=null)
    	{
    	if ( user.getPassword().equals(txtPassword.getText())){
    		config2 c = new config2();
            c.newStage(stage, lblClose, "../view/HomeView.fxml", "Test App", true, StageStyle.UNDECORATED, false);
        }
        	else{
            config2.dialog(Alert.AlertType.ERROR, "Error Login, Please Chek Username And Password");
        }
    	}
    	else{
            config2.dialog(Alert.AlertType.ERROR, "Error Login, Please Chek Username And Password");

    }

}
}