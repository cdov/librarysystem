package edu.mum.mpp.librarysys.dataaccess;

import java.util.List;
import java.util.UUID;

import edu.mum.mpp.librarysys.model.Author;

public interface IAuthor extends IBaseService<Author>{


	public Author getAuthor(String name);

	public Author getAuthor(UUID id);
}
