package edu.mum.mpp.librarysys.model;

import java.io.Serializable;
import java.util.*;

/**
 *
 */
public class Book extends Publication implements Serializable{

    /**
	 *
	 */
	private static final long serialVersionUID = -7217922824900582606L;

	/**
     * Default constructor
     */
    public Book() {
    }


	public Book(String title, int checkoutLength, String isbn, int copyAvailable, List<Copy> copies) {
		super(title, checkoutLength, isbn, copyAvailable, copies);
		// TODO Auto-generated constructor stub
	}


	public Book(String title, int checkoutLength, String isbn, int copyAvailable, List<Copy> copies
			,String publishNum,List<Author> authors) {
		super(title, checkoutLength, isbn, copyAvailable, copies);
		// TODO Auto-generated constructor stub
		this.publishNum=publishNum;
		this.authors = authors;
	}
    /**
     *
     */
    private List<Author> authors=new ArrayList<Author>();
    private String publishNum;

	public List<Author> getAuthors() {
		return authors;
	}

	public void setAuthors(List<Author> authors) {
		this.authors = authors;
	}

	public String getPublishNum() {
		return publishNum;
	}

	public void setPublishNum(String publishNum) {
		this.publishNum = publishNum;
	}



}