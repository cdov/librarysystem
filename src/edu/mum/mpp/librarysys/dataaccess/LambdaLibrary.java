package edu.mum.mpp.librarysys.dataaccess;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.function.BiFunction;

import edu.mum.mpp.librarysys.model.Author;
import edu.mum.mpp.librarysys.model.Copy;
import edu.mum.mpp.librarysys.model.LibraryMember;
import edu.mum.mpp.librarysys.model.Publication;
import edu.mum.mpp.librarysys.model.User;

public class LambdaLibrary {
	// For User  we can get User By ID

	public static BiFunction<List<User>, String, Optional<User>> getUserByRequestId= (
			list, requestId) -> list.stream()
			.filter(user -> user.getrequestId().toString().equals(requestId)).findAny();

			public static BiFunction<List<Author>, String, Optional<Author>> getAuthorByName = (
					list, name) -> list.stream()
					.filter(author -> author.toString().equals(name)).findAny();

	public static BiFunction<List<Author>, UUID, Optional<Author>> getAuthorById = (
  list, id) -> list.stream()
 .filter(author -> author.getId().equals(id)).findAny();


	/*
	public static BiFunction<List<User>, UUID, Optional<User>> getAuthorById = (
			list, id) -> list.stream()
			.filter(author -> author.getId().equals(id)).findAny();

	/*public static BiFunction<List<Book>, String, Optional<Book>> getBookByTitle = (
			list, title) -> list.stream()
			.filter(book -> book.getTitle().equals(title)).findAny();*/

	public static BiFunction<List<Publication>, String, Optional<Publication>> getPublicationByISBN = (
			list, isbn) -> list.stream()
			.filter(book -> book.getIsbn().equals(isbn)).findAny();
/*
	public static BiFunction<List<CheckoutRecord>, UUID, Optional<CheckoutRecord>> getCheckoutRecordById = (
			list, id) -> list.stream()
			.filter(checkoutRecord -> checkoutRecord.getId().equals(id))
			.findAny();

	public static BiFunction<List<CheckoutRecord>, Integer, Optional<CheckoutRecord>> getCheckoutRecordByMemberId = (
			list, memberId) -> list.stream()
			.filter(record -> record.getMember().getMemberId() == memberId)
			.findAny();

	public static BiConsumer<List<CheckoutRecord>, List<CheckoutRecordEntry>> getOverdueRecordEntries = (
			recordsList, entriesList) -> recordsList
			.forEach(record -> {
				List<CheckoutRecordEntry> recordEntries = new ArrayList<CheckoutRecordEntry>();
				record.getEntries().forEach(entry -> {
					entry.setMember(record.getMember());
					recordEntries.add(entry);
				});
				entriesList.addAll(recordEntries);
			});

/*	public static BiFunction<List<CheckoutRecordEntry>, Date, List<OverdueCopy>> getOverdueCopies = (
			list, currentTime) -> list.stream()
			.filter(entry -> entry.getDueDate().before(currentTime))
			.map(entry -> {
				OverdueCopy overdueCopy = new OverdueCopy();
				overdueCopy.setCopyNo(entry.getCopy().getCopyNo());
				overdueCopy.setPublication(entry.getCopy().getPublication());
				overdueCopy.setMember(entry.getMember());
				return overdueCopy;
			}).collect(Collectors.toList());
*/
	/*public static BiFunction<List<Copy>, String, Optional<Copy>> getCopyByCopyNo = (
			list, copyNo) -> list.stream()
			.filter(copy -> copy.getCopyNo().equals(copyNo)).findAny();
/*public static TriFunction<List<Copy>, String, String, Optional<Copy>> searchCopy = (
			list, publicationTitle, issueNumber) -> list
			.stream()
			.filter(copy -> {
				if (copy.isAvailable()) {
					Publication publication = copy.getPublication();
					if (publication.getTitle().equals(publicationTitle)
							&& issueNumber.isEmpty()) {
						return true;
					}

					if (publication.getClass() == Book.class) {
						if (((Book) publication).getIsbn().equals(
								publicationTitle)) {
							return true;
						}
					} else if (publication.getClass() == Periodical.class) {
						if (((Periodical) publication).getIssueNumber().equals(
								issueNumber)
								&& ((Periodical) publication).getTitle()
										.equals(publicationTitle)) {
							return true;
						}
					}
				}
				return false;
			}).findAny();
*/
	public static BiFunction<List<LibraryMember>, String, Optional<LibraryMember>> getLibraryMemberByName = (
			list, name) -> list.stream()
			.filter(member -> member.toString().equals(name)).findAny();

	public static BiFunction<List<LibraryMember>, Integer, Optional<LibraryMember>> getLibraryMemberByMemberId = (
			list, memberId) -> list.stream()
			.filter(member -> member.getMemberId() == memberId).findAny();
/*
	public static BiFunction<List<Periodical>, String, Optional<Periodical>> getPeriodicalByTitle = (
			list, title) -> list.stream()
			.filter(periodical -> periodical.getTitle().equals(title))
			.findAny();


	public static BiFunction<List<Periodical>, UUID, Optional<Periodical>> getPeriodicalById = (
			list, id) -> list.stream()
			.filter(periodical -> periodical.getId().equals(id)).findAny();

}*/

			public static BiFunction<List<Copy>, Boolean, Optional<Copy>> getAnAvailableCopyByISBN = (
					list,isAvailable) -> list.stream()
					.filter(copy -> copy.isAvailability()==isAvailable).findAny();

}
