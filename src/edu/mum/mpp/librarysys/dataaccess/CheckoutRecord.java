package edu.mum.mpp.librarysys.dataaccess;

import java.util.UUID;


public interface CheckoutRecord {
	public void addCheckoutRecord(CheckoutRecord checkoutRecord);

	public CheckoutRecord getCheckoutRecord(UUID id);

	public CheckoutRecord getCheckoutRecordByMemberId(int memberId);
	
	public void deleteCheckoutRecord(int index);

	public void editCheckoutRecord(CheckoutRecord currentCheckoutRecord);
}
