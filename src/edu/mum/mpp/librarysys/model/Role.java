package edu.mum.mpp.librarysys.model;

import java.io.Serializable;

/**
 * 
 */
public class Role implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 8801329332872616621L;

	/**
     * Default constructor
     */
    public Role() {
    }

    public Role(String role) {
		this.role = role;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	/**
     * 
     */
    private String role;

}