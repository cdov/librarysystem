package edu.mum.mpp.librarysys.controller;

import edu.mum.mpp.librarysys.config.config2;
import edu.mum.mpp.librarysys.dataaccess.IAuthor;
import edu.mum.mpp.librarysys.dataaccess.LibSysServiceFactory;
import edu.mum.mpp.librarysys.model.Address;
import edu.mum.mpp.librarysys.model.Author;
import edu.mum.mpp.librarysys.util.AppException;
import edu.mum.mpp.librarysys.util.CommonUtil;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

public class AuthorController implements BaseViewController {
	ScreenController myController;

    @FXML
    private TextField txtFirstName;// found
    @FXML
    private TextField txtLastName;// found
    @FXML
    private TextField txtCity;// found
    @FXML
    private TextField txtState;// found
    @FXML
    private TextField txtPhone;//found
    @FXML
    private TextField txtStreet;//found
    @FXML
    private TextField txtZip;//found

    @FXML
    private TextField txtCredential;// found
    @FXML
    private TextField txtBio;// found

    @FXML
    private AnchorPane paneCrud;        //found

	public void setScreenParent(ScreenController screenParent) {
		myController = screenParent;
	}

	@FXML
	private void initialize() {

	}


	@FXML
    private void handleSaveBtn(ActionEvent event) {

    	 config2 config = new config2();
        if (CommonUtil.isBlank(txtFirstName.getText())) {
            config2.dialog(Alert.AlertType.ERROR, "FirstName Not Empty");
            txtFirstName.requestFocus();
            return;
        }
        if (CommonUtil.isBlank(txtLastName.getText())) {
            config2.dialog(Alert.AlertType.ERROR, "LastName Not Empty");
            txtLastName.requestFocus();
            return;
        }
        if (CommonUtil.isBlank(txtCity.getText())) {
            config2.dialog(Alert.AlertType.ERROR, "City Not Empty");
            txtCity.requestFocus();
            return;
        }

        try{
        	CommonUtil.stringWithDigitsAndRangeLimitationRule("State", txtState.getText(), 2, 'A', 'Z');
        } catch(AppException e){
        	config2.dialog(Alert.AlertType.ERROR, e.getErrorMsg());
        	txtState.requestFocus();
        	return;
        }

        if (CommonUtil.isBlank(txtPhone.getText())) {
            config2.dialog(Alert.AlertType.ERROR, "Phone Not Empty");
            txtPhone.requestFocus();
            return;
        }
        if (CommonUtil.isBlank(txtStreet.getText())) {
            config2.dialog(Alert.AlertType.ERROR, "Street Not Empty");
            txtStreet.requestFocus();
            return;
        }

        try{
        	CommonUtil.numericWithDigitsLimitationRule("Zip", txtZip.getText(), 5);
        } catch(AppException e){
        	config2.dialog(Alert.AlertType.ERROR, e.getErrorMsg());
        	txtZip.requestFocus();
        	return;
        }

        IAuthor iAuthor = LibSysServiceFactory.getBizService(IAuthor.class);

        Author aAuthor = new Author(txtFirstName.getText(),txtLastName.getText(),txtPhone.getText(),
        		new Address(txtStreet.getText(),txtCity.getText(),txtState.getText(),txtZip.getText()),txtCredential.getText(),txtBio.getText());
        try {
        	iAuthor.add(aAuthor);
		} catch (Exception e) {
			config2.dialog(Alert.AlertType.ERROR, "Failed to add an Author. Error Message:" +e.getMessage());
			return;
		}
        config2.dialog(Alert.AlertType.INFORMATION, "Succeeded to add an Author.");
    }



    private void clear(){
//        txtCity.clear();
//        txtZip.clear();
//        txtLastName.clear();
//        txtStreet.clear();
//        txtFirstName.clear();
//        txtPhone.clear();
//        txtState.clear();
    }

    @FXML
    private void handleBackBtn(ActionEvent event) {
        paneCrud.setOpacity(0);
//        new FadeInUpTransition(paneTableList).play();
    }
}
