package edu.mum.mpp.librarysys.model;

import java.io.Serializable;
import java.time.LocalDate;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 */
public class CheckoutRecordEntry implements Serializable{

    /**
	 *
	 */
	private static final long serialVersionUID = 3083773767549766520L;

	/**
     * Default constructor
     */
    public CheckoutRecordEntry() {
    }

    /**
     *
     */
    private LocalDate checkoutDate;

    /**
     *
     */
    private LocalDate dueDate;

    /**
     *
     */
    private LocalDate returnDate;

	/**
     *
     */
    private double fine;
    private Copy copy;

    public CheckoutRecordEntry(LocalDate checkoutDate, LocalDate dueDate, LocalDate returnDate, double fine,
			Copy copy) {
		this.checkoutDate = checkoutDate;
		this.dueDate = dueDate;
		this.returnDate = returnDate;
		this.fine = fine;
		this.copy = copy;
	}

	public LocalDate getCheckoutDate() {
		return checkoutDate;
	}

	public void setCheckoutDate(LocalDate checkoutDate) {
		this.checkoutDate = checkoutDate;
	}

	public LocalDate getDueDate() {
		return dueDate;
	}

	public void setDueDate(LocalDate dueDate) {
		this.dueDate = dueDate;
	}

	public LocalDate getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(LocalDate returnDate) {
		this.returnDate = returnDate;
	}

	public double getFine() {
		return fine;
	}

	public void setFine(double fine) {
		this.fine = fine;
	}

	public Copy getCopy() {
		return copy;
	}

	public void setCopy(Copy copy) {
		this.copy = copy;
	}

	public StringProperty getISBNProperty() {
		return new SimpleStringProperty(this.getCopy().getPublication().getIsbn());
	}

	public StringProperty getTitleProperty() {
		return new SimpleStringProperty(this.getCopy().getPublication().getTitle());
	}
	public StringProperty getCopyIDProperty() {
		return new SimpleStringProperty(this.getCopy().getCopyId());
	}
	public ObjectProperty<LocalDate> getCheckoutDateProperty() {
		return new SimpleObjectProperty<LocalDate>(this.getCheckoutDate());
	}
	public ObjectProperty<LocalDate> getDueDateProperty() {
		return new SimpleObjectProperty<LocalDate>(this.getDueDate());
	}
	public ObjectProperty<LocalDate> getReturnDateProperty() {
		return new SimpleObjectProperty<LocalDate>(this.getReturnDate());
	}

	@Override
	public String toString() {
		return "CheckoutRecordEntry [checkoutDate=" + checkoutDate + ", dueDate=" + dueDate + ", returnDate="
				+ returnDate + ", fine=" + fine + ", copy=" + copy.getCopyId() + ", bookTitle=" + copy.getPublication().getTitle()+ "]";
	}

}