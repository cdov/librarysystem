package edu.mum.mpp.librarysys.dataaccess;

import java.util.List;

import edu.mum.mpp.librarysys.util.AppException;


public interface IBaseService<T> {
	
	public void add(T obj) throws AppException;
	
	public void delete(int index) throws AppException;

	public void edit(T editObj) throws AppException;

	public List<T> listAll() throws AppException;
}
