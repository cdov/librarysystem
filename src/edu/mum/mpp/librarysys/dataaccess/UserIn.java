package edu.mum.mpp.librarysys.dataaccess;

import java.util.List;

import edu.mum.mpp.librarysys.model.User;

public interface UserIn {
	public void deleteUser(int index);
	public void addUser(User currentUser);
	public void editUser(User currentUser);
	public User getUser(String requestId);
}
