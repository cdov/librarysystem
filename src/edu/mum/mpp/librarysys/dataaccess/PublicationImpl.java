package edu.mum.mpp.librarysys.dataaccess;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import edu.mum.mpp.librarysys.model.Book;
import edu.mum.mpp.librarysys.model.Copy;
import edu.mum.mpp.librarysys.model.LibraryMember;
import edu.mum.mpp.librarysys.model.Publication;
import edu.mum.mpp.librarysys.util.AppException;

/**
 *
 * @author Huaijin Zhou
 *
 */
public class PublicationImpl extends DataAccessBase implements IPublication {

	
	@Override
	public List<Publication> listAll() throws AppException {
		List<Publication> currentPublicationList = super.getAllItems();
		return currentPublicationList;
	}
	public void addABook(Publication publicationNew, int copyNum) throws AppException {
		List<Publication> currentPublicationList = super.getAllItems();
		if (currentPublicationList == null) {
			currentPublicationList = new ArrayList<Publication>();
		}
		if (publicationNew == null || publicationNew.getIsbn() == null) {
			throw new AppException("System expected a valid Book object or valid ISBN.");
		}
		if (checkISBNIsExisted(currentPublicationList, publicationNew.getIsbn()) != null) {
			throw new AppException("Invalid ISBN:" + publicationNew.getIsbn() + ", this ISBN already existed.");
		}
		List<Copy> copyList = makeCopiesForPublication(publicationNew, copyNum);
		publicationNew.setCopies(copyList);
		currentPublicationList.add(publicationNew);
		super.save(currentPublicationList);
	}

	@Override
	public void edit(Publication publicationNew) throws AppException{
		if (publicationNew == null || publicationNew.getIsbn() == null) {
			throw new AppException("System expected a valid Book object or valid ISBN.");
		}

		List<Publication> currentPublicationList = super.getAllItems();
		if (currentPublicationList == null) {
			currentPublicationList = new ArrayList<Publication>();
		}
		Publication publicationOld = checkISBNIsExisted(currentPublicationList, publicationNew.getIsbn());

		if (publicationOld == null) {
			throw new AppException("System can not find any book based on ISBN:" + publicationNew.getIsbn());
		}
		//copy new values from the object from front end.
		publicationOld.setCheckoutLength(publicationNew.getCheckoutLength());
		publicationOld.setTitle(publicationNew.getTitle());
		if(publicationOld instanceof Book){
			Book publicationOldBook = (Book)publicationOld;
			Book publicationNewBook = (Book)publicationNew;
			publicationOldBook.setAuthors(publicationNewBook.getAuthors());
			publicationOldBook.setPublishNum(publicationNewBook.getPublishNum());
		}
		super.save(currentPublicationList);
	}
	public void addACopy2Book(String isbn, Copy copy) throws AppException {
		List<Publication> currentPublicationList = super.getAllItems();
		if (currentPublicationList == null) {
			currentPublicationList = new ArrayList<Publication>();
		}
		Publication publication = checkISBNIsExisted(currentPublicationList, isbn);
		if (publication == null) {
			throw new AppException("System can not find any book based on ISBN:" + isbn);
		}
		List<Copy> copyList = publication.getCopies();
		if (copyList == null) {
			copyList = new ArrayList<Copy>();
		}
		String nextCopyId = isbn + "_" + copyList.size() + 1;
		if (copy == null) {
			copy = new Copy();
		}
		copy.setCopyId(nextCopyId);
		copy.setAvailability(true);
		copy.setPublication(publication);
		copyList.add(copy);
		publication.setCopies(copyList);
		//currentPublicationList.add(publication);
		super.save(currentPublicationList);
	}

	private List<Copy> makeCopiesForPublication(Publication publicationNew, int copyNum) {
		List<Copy> copyList = new ArrayList<Copy>();
		String nextCopyId = null;
		Copy copy = null;
		for (int i = 0; i < copyNum; i++) {
			nextCopyId = publicationNew.getIsbn() + "_" + (i + 1);
			copy = new Copy();
			copy.setCopyId(nextCopyId);
			copy.setAvailability(true);
			copy.setPublication(publicationNew);
			copyList.add(copy);
		}
		return copyList;
	}

	/**
	 * if returns a nonempty object, it means that is an available copy of the
	 * book with an ISBN specified in input parameter.
	 * if returns null, means there is no any available copies of the book.
	 * And in this method, system also will mark the isAvailability of this copy with false,
	 * it means this copy has been checked out.
	 * @param isbn
	 * @return
	 * @throws AppException
	 */
	public Copy checkoutACopyByISBN(String isbn) throws AppException {
		List<Publication> currentPublicationList = super.getAllItems();
		Publication publication = checkISBNIsExisted(currentPublicationList, isbn);

		if (publication == null) {
			throw new AppException("System can not find any book based on ISBN:" + isbn);
		}
		List<Copy> copyList = publication.getCopies();
		if (copyList == null) {
			throw new AppException("System expected valid copies of ISBN:" + isbn);
		}
		Copy rtnCopy = null;
		for (Copy aCopy : copyList) {
			if (aCopy.isAvailability()) {
				rtnCopy = aCopy;
				aCopy.setAvailability(false);// do checkout process
				super.save(currentPublicationList);// persist it into an object
				break;
			}
		}
		return rtnCopy;
	}

	private Publication checkISBNIsExisted(List<Publication> currentPublicationList, String isbn) throws AppException {
		if (currentPublicationList == null) {
			throw new AppException("System Error. The publication list is null.");
		}
		if (isbn == null || isbn.equals("")) {
			throw new AppException("System Error. The ISBN is empty.");
		}
		Optional<Publication> aPublication = LambdaLibrary.getPublicationByISBN.apply(currentPublicationList, isbn);
		if (!aPublication.isPresent() && aPublication!= null) {
			return null;
		}else{
			return aPublication.get();
		}
	}

	public Publication getByISBN(String isbn){
		List<Publication> publicatons=super.getAllItems();
		Optional<Publication> publication=publicatons.stream().filter(pub->pub.getIsbn().equals(isbn)).findFirst();
		return publication.isPresent()?publication.get():null;
	}

	public void updateAvailabilityCopy(String isbn,Copy copy,boolean bool) throws AppException {
		List<Publication> currentPublicationList = super.getAllItems();
		Publication publication = checkISBNIsExisted(currentPublicationList, isbn);

		if (publication == null) {
			throw new AppException("System can not find any book based on ISBN:" + isbn);
		}
		List<Copy> copyList = publication.getCopies();
		if (copyList == null) {
			throw new AppException("System expected valid copies of ISBN:" + isbn);
		}
		for (Copy aCopy : copyList) {
			if (aCopy.getCopyId().equals(copy.getCopyId())) {
				aCopy.setAvailability(bool);// do checkout process
				super.save(currentPublicationList);// persist it into an object
				break;
			}
		}
	}

	public void addNewCopies2Book(String isbn, int copyNumber) throws AppException {
		List<Publication> currentPublicationList = super.getAllItems();
		if (currentPublicationList == null) {
			currentPublicationList = new ArrayList<Publication>();
		}
		Publication publication = checkISBNIsExisted(currentPublicationList, isbn);
		if (publication == null) {
			throw new AppException("System can not find any book based on ISBN:" + isbn);
		}
		List<Copy> copyList = publication.getCopies();
		if (copyList == null) {
			copyList = new ArrayList<Copy>();
		}
		int currentSize = copyList.size();
		String nextCopyId = "";
        Copy aNewCopy = null;
		for(int i=0;i<copyNumber;i++){
			aNewCopy = new Copy();
			nextCopyId = isbn + "_" + (currentSize + i+1);
			aNewCopy.setCopyId(nextCopyId);
			aNewCopy.setAvailability(true);
			aNewCopy.setPublication(publication);
			copyList.add(aNewCopy);
		}

		publication.setCopies(copyList);
		//currentPublicationList.add(publication);
		super.save(currentPublicationList);
	}
	@Override
	public void add(Publication obj) throws AppException {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void delete(int index) throws AppException {
		// TODO Auto-generated method stub
		List<Publication> allMembers = getAllItems();
		allMembers.remove(index);
		save(allMembers);
	}	

	
}
