package edu.mum.mpp.librarysys.model;

import java.io.Serializable;
import java.util.UUID;

/**
 * 
 */
public class Person implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = -4649026268210797342L;
	/**
     * 
     */
    private String firstName;
    /**
     * 
     */
    private String lastName;

    /**
     * 
     */
    private String phoneNumber;
    /**
     * Default constructor
     */
    private Address address;
    protected UUID id;

	public UUID getId() {
		return id;
	}
 public Person() {
    }



    public Person(String firstName, String lastName, String phoneNumber,Address address) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.phoneNumber = phoneNumber;
		this.address=address;
	}



	public String getFirstName() {
		return firstName;
	}



	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}



	public String getLastName() {
		return lastName;
	}



	public void setLastName(String lastName) {
		this.lastName = lastName;
	}



	public String getPhoneNumber() {
		return phoneNumber;
	}



	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}



	public Address getAddress() {
		return address;
	}



	public void setAddress(Address address) {
		this.address = address;
	}

	


}