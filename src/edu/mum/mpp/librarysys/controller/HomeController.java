
package edu.mum.mpp.librarysys.controller;
import edu.mum.mpp.librarysys.ui.MainApp;

import java.util.List;

import edu.mum.mpp.librarysys.config.config2;
import edu.mum.mpp.librarysys.model.Role;
import edu.mum.mpp.librarysys.model.User;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class HomeController {
	private static final String LibraryMember = "Library Members";
	private static final String Books = "Books";
	private static final String Authors = "Authors";
	private static final String Copies = "Copies";
	private static final String Checkout_Book = "Checkout Book";

	private static final String[] submenuStrArr = new String[]{LibraryMember,Books,Authors,Copies,Checkout_Book};
	private static final String[] submenuStrboth = new String[]{LibraryMember,Books,Authors,Copies,Checkout_Book};
	private static final String[] submenuforadmin = new String[]{LibraryMember,Books,Authors,Copies};
	private static final String[] submenuofLibrarian= new String[]{Checkout_Book};

	MainApp mainApp;
	    User user;
	    Role role;
	public void setMainApp(MainApp mainApp) {
	    this.mainApp = mainApp;

	}

	// code for retrive window
	   	@FXML
	    private Button close;
	    @FXML
	    private Button maximize;
	    @FXML
	    private Button minimize;
	    @FXML
	    private Button resize;
	    @FXML
	    private Button fullscreen;
	    @FXML
	    private Label title;
	    Stage stage;
	    Rectangle2D rec2;
	    Double w,h;
	    @FXML
	    private ListView<String> listMenu;
	    @FXML
	    private AnchorPane paneData;
	    config2 con = new config2();
	    @FXML
	    private Button btnLogout;
	    /**
	     * Initializes the controller class.
	     * @param url
	     * @param rb
	     */
	    @FXML
	    private void initialize() {
	        rec2 = Screen.getPrimary().getVisualBounds();
	        w = 0.1;
	        h = 0.1;
	        // need Implement to check of rules


	        // Check Who are you
	        User m=new User();
	        m=Usercontroller.user;
    List<Role> roles=m.getRoles();
     for(Role role:roles)
     {
    	System.out.print(role.getRole());
	  if(role.getRole().equals("ADMIN"))
	   {
	    listMenu.getItems().addAll(submenuforadmin);
	   }
  	   else if(role.getRole().equals("BOTH"))
    	   {
    		   listMenu.getItems().addAll(submenuStrboth);
    	   }

  	 else if(role.getRole().equals("LIBRARIAN"))
	   {
		   listMenu.getItems().addAll(submenuofLibrarian);

  	   }
     }
	        Platform.runLater(() -> {
	            stage = (Stage) maximize.getScene().getWindow();
	            stage.setMaximized(true);
	            stage.setHeight(rec2.getHeight());
	            maximize.getStyleClass().add("decoration-button-restore");
	            resize.setVisible(false);
	            listMenu.getSelectionModel().select(0);
	         //   con.loadAnchorPane(paneData, "../view/Librarymemberview.fxml");
	            listMenu.requestFocus();
	        });

	       }

	    @FXML
	    private void aksiMaximized(ActionEvent event) {
	        stage = (Stage) maximize.getScene().getWindow();
	        if (stage.isMaximized()) {
	            if (w == rec2.getWidth() && h == rec2.getHeight()) {
	                stage.setMaximized(false);
	                stage.setHeight(600);
	                stage.setWidth(800);
	                stage.centerOnScreen();
	                maximize.getStyleClass().remove("decoration-button-restore");
	                resize.setVisible(true);
	            }else{
	                stage.setMaximized(false);
	                maximize.getStyleClass().remove("decoration-button-restore");
	                resize.setVisible(true);
	            }

	        }else{
	            stage.setMaximized(true);
	            stage.setHeight(rec2.getHeight());
	            maximize.getStyleClass().add("decoration-button-restore");
	            resize.setVisible(false);
	        }
	    }

	    @FXML
	    private void aksiminimize(ActionEvent event) {
	        stage = (Stage) minimize.getScene().getWindow();
	        if (stage.isMaximized()) {
	            w = rec2.getWidth();
	            h = rec2.getHeight();
	            stage.setMaximized(false);
	            stage.setHeight(h);
	            stage.setWidth(w);
	            stage.centerOnScreen();
	            Platform.runLater(() -> {
	                stage.setIconified(true);
	            });
	        }else{
	            stage.setIconified(true);
	        }
	    }

	    @FXML
	    private void aksiResize(ActionEvent event) {
	    }

	    @FXML
	    private void aksifullscreen(ActionEvent event) {
	        stage = (Stage) fullscreen.getScene().getWindow();
	        if (stage.isFullScreen()) {
	            stage.setFullScreen(false);
	        }else{
	            stage.setFullScreen(true);
	        }
	    }

	    @FXML
	    private void aksiClose(ActionEvent event) {
	        Platform.exit();
	        System.exit(0);
	    }

	    @FXML
	    /**
	     * respond the click event of menu item
	     * @param event
	     */
	    private void aksiKlikListMenu(MouseEvent event) {
	    	// try to put different views based on rules
	    	if(true){
	    		System.out.println(listMenu.getSelectionModel().getSelectedItem());
	        switch(listMenu.getSelectionModel().getSelectedItem()){
	            case LibraryMember:{
	                con.loadAnchorPane(paneData, "../view/Librarymemberview.fxml");
	            }break;
	            case Books:{
	                con.loadAnchorPane(paneData, "../view/BookViewNew.fxml");
	            }break;
	            case Copies:{
	                con.loadAnchorPane(paneData, "../view/CopyView.fxml");
	            }break;
	            case Authors:{
	                con.loadAnchorPane(paneData, "../view/AuthorView.fxml");
	            }break;
	            case Checkout_Book:{
	                con.loadAnchorPane(paneData, "../view/CheckoutBook.fxml");
	                break;
	            }
	        }
	    	}

	    }

	    @FXML
	    private void aksiLogout(ActionEvent event) {

	  	 config2 config = new config2();
	       config.newStage2(stage, btnLogout, "../view/login.fxml", "Sample Apps", true, StageStyle.UNDECORATED, false);
	    }}

