package edu.mum.mpp.librarysys.model;

import java.io.Serializable;

/**
 * 
 */
public class Periodical extends Publication implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1709522942836019995L;

	/**
     * Default constructor
     */
    public Periodical() {
    }

    public long getIssueNumber() {
		return issueNumber;
	}

	public void setIssueNumber(long issueNumber) {
		this.issueNumber = issueNumber;
	}

	/**
     * 
     */
    private long issueNumber;

	public Periodical(long issueNumber) {
		this.issueNumber = issueNumber;
	}

}