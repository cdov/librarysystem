package edu.mum.mpp.librarysys.util;

public class AppException extends Exception {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	protected String errorMsg = "";
	protected Throwable e;

	public AppException(String errorMsg, Throwable e) {
		super();
		this.errorMsg = errorMsg;
		this.e = e;
	}

	public AppException(String errorMsg) {
		super();
		this.errorMsg = errorMsg;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public Throwable getE() {
		return e;
	}

	public void setE(Throwable e) {
		this.e = e;
	}

}
