package edu.mum.mpp.librarysys.model;

import java.io.Serializable;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 */
public class Copy implements Serializable{

    /**
	 *
	 */
	private static final long serialVersionUID = 1767058201154698287L;
	/**
     * Default constructor
     */
    public Copy() {
    }

    /**
     *
     */
    private String copyId;

    /**
     *
     */
    private boolean availability;
    private Publication publication;
	public String getCopyId() {
		return copyId;
	}
	public void setCopyId(String copyId) {
		this.copyId = copyId;
	}
	public boolean isAvailability() {
		return availability;
	}
	public void setAvailability(boolean availability) {
		this.availability = availability;
	}
	public Publication getPublication() {
		return publication;
	}
	public void setPublication(Publication publication) {
		this.publication = publication;
	}
	public Copy(String copyId, boolean availability, Publication publication) {
		this.copyId = copyId;
		this.availability = availability;
		this.publication = publication;
	}
	public StringProperty getIsAvailableProperty() {
		return new SimpleStringProperty(availability ? "Yes" : "No");
	}

	public StringProperty getCopyNoProperty() {
		return new SimpleStringProperty(this.copyId);
	}

}