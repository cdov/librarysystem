package edu.mum.mpp.librarysys.controller;


import java.time.LocalDate;
import java.util.List;

import edu.mum.mpp.librarysys.dataaccess.ILibraryMember;
import edu.mum.mpp.librarysys.dataaccess.IPublication;
import edu.mum.mpp.librarysys.dataaccess.LibSysServiceFactory;
import edu.mum.mpp.librarysys.model.CheckoutRecord;
import edu.mum.mpp.librarysys.model.CheckoutRecordEntry;
import edu.mum.mpp.librarysys.model.Copy;
import edu.mum.mpp.librarysys.model.LibraryMember;
import edu.mum.mpp.librarysys.model.Publication;
import edu.mum.mpp.librarysys.util.AppException;
import edu.mum.mpp.librarysys.util.GlobalFunction;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

public class CheckoutController {
	  	@FXML
	    private TextField txtMemberID;

	    @FXML
	    private Label lblFullName;

	    @FXML
	    private Label lblISBN;

	    @FXML
	    private TableView<Copy> TVBookCopy;

	    @FXML
	    private TableColumn<Copy, String> copyId;

	    @FXML
	    private TableColumn<Copy, String> availability;

	    @FXML
	    private Button btnMemberSearch;

	    @FXML
	    private Label lblMemberID;

	    @FXML
	    private Button btnBookSearch;

	    @FXML
	    private TextField txtISBN;

	    @FXML
	    private Label lblTitle;

	    @FXML
	    private TableView<CheckoutRecordEntry> TVCheckoutRecord;

	    @FXML
	    private TableColumn<CheckoutRecordEntry, String> isbn;

	    @FXML
	    private TableColumn<CheckoutRecordEntry, String> copyIdT;
	    
	    @FXML
	    private TableColumn<CheckoutRecordEntry,String> title;

	    @FXML
	    private TableColumn<CheckoutRecordEntry,LocalDate> dueDate;

	    @FXML
	    private TableColumn<CheckoutRecordEntry,LocalDate> checkoutDate;

	    @FXML
	    private TableColumn<CheckoutRecordEntry,LocalDate> returnDate;
	    
	    @FXML
	    private Label lblCheckoutLength;

	    private ILibraryMember libraryMemberService=LibSysServiceFactory.getBizService(ILibraryMember.class);
	    private IPublication publicationService=LibSysServiceFactory.getBizService(IPublication.class);

	    @FXML
	    private void initialize() {
	        // Initialize the person table with the two columns.
	    	copyId.setCellValueFactory(cellData -> cellData.getValue().getCopyNoProperty());
	    	availability.setCellValueFactory(cellData -> cellData.getValue().getIsAvailableProperty());
	    	isbn.setCellValueFactory(cellData -> cellData.getValue().getISBNProperty());
	    	copyIdT.setCellValueFactory(cellData -> cellData.getValue().getCopyIDProperty());
	    	title.setCellValueFactory(cellData -> cellData.getValue().getTitleProperty());
	    	dueDate.setCellValueFactory(cellData -> cellData.getValue().getDueDateProperty());
	    	checkoutDate.setCellValueFactory(cellData -> cellData.getValue().getCheckoutDateProperty());
	    	returnDate.setCellValueFactory(cellData -> cellData.getValue().getReturnDateProperty());
	    }


	    @FXML
		private void searchMember(ActionEvent event) {
	    	try {
	    		long memberId=Long.parseLong(txtMemberID.getText());
	    		LibraryMember member=libraryMemberService.getLibraryMember(memberId);
		    	if(member==null){
		    		Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("No Member Found");
					alert.setHeaderText(null);
					alert.setContentText("No Member Found, please retype!");
					alert.showAndWait();
					txtMemberID.setText("");
					txtMemberID.requestFocus();
		    	}
		    	lblMemberID.setText(member.getMemberId()+"");
		    	lblFullName.setText(member.getFirstName()+" "+member.getLastName());
//		    	txtMemberID.setText("");
		    	if(member.getCheckoutRecord()!=null)
		    		TVCheckoutRecord.setItems(FXCollections.observableArrayList(member.getCheckoutRecord().getCheckoutRecordEntries()));

			} catch (Exception e) {
				GlobalFunction.showAlert("Error", "Invalid input of member id!");
				txtMemberID.setText("");
				txtMemberID.requestFocus();
			}

	    }

	    @FXML
		private void printCheckoutRecords() {
	    	try {
	    		long memberId=Long.parseLong(txtMemberID.getText());
	    		LibraryMember member=libraryMemberService.getLibraryMember(memberId);
		    	if(member==null){
		    		Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("No Member Found");
					alert.setHeaderText(null);
					alert.setContentText("No Member Found, please retype!");
					alert.showAndWait();
					txtMemberID.setText("");
					txtMemberID.requestFocus();
					return;
		    	}

		    	if(member.getCheckoutRecord()!=null){
		    		System.out.println("Checkout Record and Checkout Record Entries:");
		    		System.out.println("Member Name: " +member.getLastName()+", "+ member.getFirstName());
		    		System.out.println("NO.\tCopyId\tBookTitle\tCheckoutDate\tDueDate\tReturnDate");
		    		List<CheckoutRecordEntry> entries = member.getCheckoutRecord().getCheckoutRecordEntries();
                    if(entries!=null){
                    	int i =1;
                    	for(CheckoutRecordEntry e : entries){
                    		System.out.println(i+"\t"+e.getCopy().getCopyId()+"\t"+e.getCopy().getPublication().getTitle()+"\t"+e.getCheckoutDate()+"\t"+e.getDueDate()+"\t"+e.getDueDate());
                    		i++;
                    	}
                    }
		    	}
			} catch (Exception e) {
				GlobalFunction.showAlert("Error", "Invalid input of member id!");
				txtMemberID.setText("");
				txtMemberID.requestFocus();
			}

	    }

	    @FXML
	    private void searchBook(){
	    	Publication pub=publicationService.getByISBN(txtISBN.getText());
	    	if(pub==null){
	    		Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("No Publication Found");
				alert.setHeaderText(null);
				alert.setContentText("No Publication Found, please retype!");
				alert.showAndWait();
				txtISBN.setText("");
				txtISBN.requestFocus();
	    	}
	    	lblISBN.setText(pub.getIsbn());
	    	lblTitle.setText(pub.getTitle());
	    	lblCheckoutLength.setText(pub.getCheckoutLength()+" Day");
	    	txtISBN.setText("");
	    	TVBookCopy.setItems(FXCollections.observableArrayList(pub.getCopies()));

	    }
	    @FXML
	    private void processCheckout(){
	    	int selectedIndex = TVBookCopy.getSelectionModel().getSelectedIndex();
			if (selectedIndex >= 0) {
				Copy copy=TVBookCopy.getItems().get(selectedIndex);

				if(copy.isAvailability()){
					if(lblMemberID.getText().isEmpty()){
						GlobalFunction.showAlert("No member", "You need to enter a member in order to checkout book!");
					}
					LibraryMember member=libraryMemberService.getLibraryMember(Long.parseLong(lblMemberID.getText()));
					CheckoutRecordEntry entry=new CheckoutRecordEntry();
					entry.setCopy(copy);
					entry.setCheckoutDate(LocalDate.now());
					entry.setDueDate(computeDueDate(copy));
					if(member.getCheckoutRecord()==null){
						member.setCheckoutRecord(new CheckoutRecord());
					}
					member.getCheckoutRecord().addEntry(entry);
					
					try {
						libraryMemberService.edit(member);
						publicationService.updateAvailabilityCopy(copy.getPublication().getIsbn(), copy,false);
					} catch (AppException e) {
						// TODO Auto-generated catch block
						GlobalFunction.showAlert("Error", e.getMessage());
					}
					updateTableView(copy,member,selectedIndex);
				}
				else{
					GlobalFunction.showAlert("Not available", "Select copy book not available!");
				}
			} else {
				// Nothing selected
				GlobalFunction.showAlert("No Copy Selected", "Please select a Copy in the table.");
			}

	    }
	    
	    @FXML
	    private void processCheckinRecord(){
	    	int selectedIndex = TVCheckoutRecord.getSelectionModel().getSelectedIndex();
			if (selectedIndex >= 0) {
				CheckoutRecordEntry entry=TVCheckoutRecord.getItems().get(selectedIndex);
				
				if(entry.getReturnDate()==null){
					if(lblMemberID.getText().isEmpty()){
						GlobalFunction.showAlert("No member", "You need to enter a member in order to checkout book!");
					}
					LibraryMember member=libraryMemberService.getLibraryMember(Long.parseLong(lblMemberID.getText()));
					for (CheckoutRecordEntry curEntry : member.getCheckoutRecord().getCheckoutRecordEntries()) {
						if(curEntry.getCopy().getCopyId().equals(entry.getCopy().getCopyId())
							&& curEntry.getReturnDate()==null){
							curEntry.setReturnDate(LocalDate.now());
						}
					}
					
					
					try {
						libraryMemberService.edit(member);
						publicationService.updateAvailabilityCopy(entry.getCopy().getPublication().getIsbn(), entry.getCopy(),true);
					} catch (AppException e) {
						// TODO Auto-generated catch block
						GlobalFunction.showAlert("Error", e.getMessage());
					}
//					updateTableView(copy,member,selectedIndex);
					TVCheckoutRecord.setItems(FXCollections.observableArrayList(member.getCheckoutRecord().getCheckoutRecordEntries()));
					Publication pub=publicationService.getByISBN(lblISBN.getText());
			    	if(pub!=null){
			    		TVBookCopy.setItems(FXCollections.observableArrayList(pub.getCopies()));
			    	}			    	
				}
				else{
					GlobalFunction.showAlert("Error", "Select copy book already check in!");
				}
			} else {
				// Nothing selected
				GlobalFunction.showAlert("No Copy Selected", "Please select a Checkout book in the table.");
			}

	    	
	    }
	    private LocalDate computeDueDate(Copy copy){
	    	int day=copy.getPublication().getCheckoutLength();

			return LocalDate.now().plusDays(day);
	    }
	    private void updateTableView(Copy copy,LibraryMember member,int index){
	    	TVCheckoutRecord.setItems(FXCollections.observableArrayList(member.getCheckoutRecord().getCheckoutRecordEntries()));
	    	copy.setAvailability(false);
	    	TVBookCopy.getItems().set(index, copy);
	    }
}
