package edu.mum.mpp.librarysys.controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import edu.mum.mpp.librarysys.animation.FadeInUpTransition;
import edu.mum.mpp.librarysys.config.config2;
import edu.mum.mpp.librarysys.dataaccess.ILibraryMember;
import edu.mum.mpp.librarysys.dataaccess.LibSysServiceFactory;
import edu.mum.mpp.librarysys.dataaccess.LibraryMemberImpl;
import edu.mum.mpp.librarysys.model.Address;
import edu.mum.mpp.librarysys.model.LibraryMember;
import edu.mum.mpp.librarysys.util.GlobalFunction;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

public class LibraryMemberController implements Initializable {

	// initialize the search members
	@FXML
	private TextField txtMembersearch;

	@FXML
	private Label lblFullName;

	@FXML
	private Button btnMemberSearch;

	@FXML
	private Button btnMemberedit;

	@FXML
	private Label lblMemberID;

	@FXML
	private void searchMember(ActionEvent event) {
		try {
			long memberId = Long.parseLong(txtMembersearch.getText());
			LibraryMemberImpl libraryMemberImpl = new LibraryMemberImpl();
			LibraryMember member = libraryMemberImpl.getLibraryMember(memberId);
			if (member == null) {
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("No Member Found");
				alert.setHeaderText(null);
				alert.setContentText("No Member Found, please retype!");
				alert.showAndWait();
				txtMembersearch.setText("");
				txtMembersearch.requestFocus();
			}
			lblMemberID.setText(member.getMemberId() + "");
			lblFullName.setText(member.getFirstName() + " " + member.getLastName());
			txtMembersearch.setText("");

		} catch (Exception e) {
			GlobalFunction.showAlert("Error", e.getMessage());
			txtMembersearch.setText("");
			txtMembersearch.requestFocus();
		}

	}

	@FXML
	private void EditMemberlb(ActionEvent event) {
		try {
			if (lblMemberID.getText() != null) {

				LibraryMemberImpl libraryMemberImpl = new LibraryMemberImpl();
				LibraryMember member = libraryMemberImpl.getLibraryMember(Long.valueOf(lblMemberID.getText()));
				txtMemberId.setText(String.valueOf(member.getMemberId()));
				txtCity.setText(member.getAddress().getCity());
				txtFirstName.setText(member.getFirstName());
				txtLastName.setText(member.getLastName());
				txtPhone.setText(member.getPhoneNumber());
				txtState.setText(member.getAddress().getState());
				txtStreet.setText(member.getAddress().getStreet());
				txtZip.setText(member.getAddress().getZip());
				paneTabel.setOpacity(0);
				new FadeInUpTransition(paneCrud).play();
				Platform.runLater(() -> {
					btnEdit.setDisable(false);
					btnSave.setDisable(true);
					txtMemberId.setDisable(true);
				});
			}
		} catch (Exception e) {
		}
	}
	//

	@FXML
	private TableView<LibraryMember> tableData;
	@SuppressWarnings("rawtypes")
	@FXML
	private TableColumn colAction;
	@FXML
	private TableColumn<LibraryMember, Long> colMemberId;// found
	@FXML
	private TableColumn<LibraryMember, String> colZip;// found
	@FXML
	private TableColumn<LibraryMember, String> colFirstName;// found
	@FXML
	private TableColumn<LibraryMember, String> colCity; // found
	@FXML
	private TableColumn<LibraryMember, String> colState;// found
	@FXML
	private TableColumn<LibraryMember, String> colPhone;// found
	@FXML
	private TableColumn<LibraryMember, String> colStreet;// found
	@FXML
	private Button btnNew;
	@FXML
	private AnchorPane paneTabel; // found
	@FXML
	private AnchorPane paneCrud; // found
	@FXML
	private TextField txtMemberId; // found
	@FXML
	private TextField txtFirstName;// found
	@FXML
	private TextField txtLastName;// found
	@FXML
	private TextField txtCity;// found
	@FXML
	private TextField txtState;// found
	@FXML
	private TextField txtPhone;// found
	@FXML
	private TextField txtStreet;// found
	@FXML
	private TextField txtZip;// found
	@FXML
	private Button btnSave; // found
	@FXML
	private Button btnEdit; // found
	@FXML
	private Button btnBack; // found
	Integer status;
	private ImageView imgLoad;
	@FXML
	private ProgressBar bar;

	/**
	 * Initializes the controller class.
	 * 
	 * @param url
	 * @param rb
	 */
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		Platform.runLater(() -> {

			// check for validation
			txtMemberId.addEventFilter(KeyEvent.KEY_TYPED, new EventHandler<KeyEvent>() {
				@Override
				public void handle(KeyEvent inputevent) {
					if (!inputevent.getCharacter().matches("\\d")) {
						if (inputevent.getCode().equals(KeyCode.UNDEFINED)) {
							inputevent.consume();
						} else {
							config2.dialog(Alert.AlertType.INFORMATION, "your membeId must be number");
							inputevent.consume();
						}
					}
				}
			});
			status = 0;

			txtPhone.addEventFilter(KeyEvent.KEY_TYPED, new EventHandler<KeyEvent>() {
				@Override
				public void handle(KeyEvent inputevent) {
					if (!inputevent.getCharacter().matches("\\d")) {

						if (inputevent.getCode().equals(KeyCode.UNDEFINED)) {

							inputevent.consume();
						} else {
							config2.dialog(Alert.AlertType.INFORMATION, "your phone must be number");
							inputevent.consume();
						}
					}
				}
			});
			status = 0;

			txtZip.addEventFilter(KeyEvent.KEY_TYPED, new EventHandler<KeyEvent>() {
				@Override
				public void handle(KeyEvent inputevent) {
					if (!inputevent.getCharacter().matches("\\d")) {
						if (inputevent.getCode().equals(KeyCode.UNDEFINED)) {
							inputevent.consume();
						} else {
							config2.dialog(Alert.AlertType.INFORMATION, "your zip must be number");
							inputevent.consume();
						}
					}
				}
			});
			status = 0;

			ILibraryMember libraryMemberIpml = LibSysServiceFactory.getBizService(ILibraryMember.class);
			List<LibraryMember> members;
			try {
				members = libraryMemberIpml.listAll();
				colMemberId.setCellValueFactory(new PropertyValueFactory<LibraryMember, Long>("memberId"));
				colZip.setCellValueFactory(cellData -> cellData.getValue().getAddress().getZipProperty());
				colFirstName.setCellValueFactory(new PropertyValueFactory<LibraryMember, String>("firstName"));
				colCity.setCellValueFactory(cellData -> cellData.getValue().getAddress().getCityProperty());
				colState.setCellValueFactory(cellData -> cellData.getValue().getAddress().getStateProperty());
				colPhone.setCellValueFactory(new PropertyValueFactory<LibraryMember, String>("phoneNumber"));
				colStreet.setCellValueFactory(cellData -> cellData.getValue().getAddress().getStreetProperty());

				tableData.setItems(FXCollections.observableArrayList(members));
				status = 1;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			
		});
		paneCrud.setOpacity(0);
		new FadeInUpTransition(paneTabel).play();
	}

	private void clear() {
		txtCity.clear();
		txtZip.clear();
		txtLastName.clear();
		txtStreet.clear();
		txtFirstName.clear();
		txtPhone.clear();
		txtState.clear();
		txtMemberId.clear();
	}

	@FXML
	private void keyState(KeyEvent e) {
		if (txtState.getText().length() > 2) {
			if (e.getCode().equals(KeyCode.UNDEFINED)) {
				// doSomething();
			} else {
				config2.dialog(Alert.AlertType.INFORMATION, "State Must 2 Char");
				txtState.clear();
			}
		}
	}

	@FXML
	private void MemberIdsearch(KeyEvent e) {
		LibraryMemberImpl libraryMemberImpl = new LibraryMemberImpl();
		LibraryMember member = libraryMemberImpl.getLibraryMember(Long.valueOf(txtMemberId.getText()));

		if (Long.valueOf(txtMemberId.getText()).equals(member.getMemberId())) {
			txtMemberId.clear();
			config2.dialog(Alert.AlertType.INFORMATION, "member found");
			txtState.clear();
		}
	}

	@FXML
	private void aksiKlikTableData(MouseEvent event) {
		if (status == 1) {
			try {
				LibraryMember klik = tableData.getSelectionModel().getSelectedItem();
				txtMemberId.setText(String.valueOf(klik.getMemberId()));
				txtCity.setText(klik.getAddress().getCity());
				txtFirstName.setText(klik.getFirstName());
				txtLastName.setText(klik.getLastName());
				txtPhone.setText(klik.getPhoneNumber());
				txtState.setText(klik.getAddress().getState());
				txtStreet.setText(klik.getAddress().getStreet());
				txtZip.setText(klik.getAddress().getZip());
				paneTabel.setOpacity(0);
				new FadeInUpTransition(paneCrud).play();
				Platform.runLater(() -> {
					btnEdit.setDisable(false);
					btnSave.setDisable(true);
					txtMemberId.setDisable(true);
				});
			} catch (Exception e) {
			}
		}
	}

	@FXML
	private void aksiNew(ActionEvent event) {
		paneTabel.setOpacity(0);
		new FadeInUpTransition(paneCrud).play();
		Platform.runLater(() -> {
			clear();
			btnEdit.setDisable(true);
			btnSave.setDisable(false);
			txtMemberId.setDisable(false);

			// auto();
		});
	}

	@FXML
	private void aksiEdit(ActionEvent event) {
		if (txtFirstName.getText().isEmpty()) {
			config2.dialog(Alert.AlertType.ERROR, "Name Not Empty");
			txtFirstName.requestFocus();
		} else if (txtLastName.getText().isEmpty()) {
			config2.dialog(Alert.AlertType.ERROR, "Name Not Empty");
			txtLastName.requestFocus();
		} else if (txtCity.getText().isEmpty()) {
			config2.dialog(Alert.AlertType.ERROR, "City Not Empty");
			txtCity.requestFocus();
		} else if (txtState.getText().isEmpty()) {
			config2.dialog(Alert.AlertType.ERROR, "State Not Empty");
			txtState.requestFocus();
		} else if (txtPhone.getText().isEmpty()) {
			config2.dialog(Alert.AlertType.ERROR, "Phone Not Empty");
			txtPhone.requestFocus();
		} else if (txtStreet.getText().isEmpty()) {
			config2.dialog(Alert.AlertType.ERROR, "Street Not Empty");
			txtStreet.requestFocus();
		} else if (txtZip.getText().isEmpty()) {
			config2.dialog(Alert.AlertType.ERROR, "Zip Not Empty");
			txtZip.requestFocus();
		} else {

			LibraryMember member = new LibraryMember();
			member.setFirstName(txtFirstName.getText());
			member.setLastName(txtLastName.getText());
			member.setPhoneNumber(txtPhone.getText());
			member.setMemberId(Long.valueOf(txtMemberId.getText()));
			member.setFirstName(txtFirstName.getText());
			member.setLastName(txtLastName.getText());
			member.setPhoneNumber(txtPhone.getText());
			Address address = new Address(txtStreet.getText(), txtCity.getText(), txtState.getText(), txtZip.getText());
			member.setAddress(address);

			LibraryMemberImpl libraryMemberImpl = new LibraryMemberImpl();
			libraryMemberImpl.edit(member);
			List<LibraryMember> members = libraryMemberImpl.getAllItems();
			tableData.setItems(FXCollections.observableArrayList(members));
			clear();
			// // selectData();
			// auto();
			config2.dialog(Alert.AlertType.INFORMATION, "Success Edit Data. . .");
		}
	}

	@FXML
	private void aksiSave(ActionEvent event) {
		if (txtMemberId.getText().isEmpty()) {
			config2.dialog(Alert.AlertType.ERROR, "ID Not Empty");
			txtMemberId.requestFocus();
		} else if (txtFirstName.getText().isEmpty()) {
			config2.dialog(Alert.AlertType.ERROR, "Name Not Empty");
			txtFirstName.requestFocus();
		} else if (txtLastName.getText().isEmpty()) {
			config2.dialog(Alert.AlertType.ERROR, "Name Not Empty");
			txtLastName.requestFocus();
		} else if (txtCity.getText().isEmpty()) {
			config2.dialog(Alert.AlertType.ERROR, "City Not Empty");
			txtCity.requestFocus();
		} else if (txtState.getText().isEmpty()) {
			config2.dialog(Alert.AlertType.ERROR, "State Not Empty");
			txtState.requestFocus();
		} else if (txtPhone.getText().isEmpty()) {
			config2.dialog(Alert.AlertType.ERROR, "Phone Not Empty");
			txtPhone.requestFocus();
		} else if (txtStreet.getText().isEmpty()) {
			config2.dialog(Alert.AlertType.ERROR, "Street Not Empty");
			txtStreet.requestFocus();
		} else if (txtZip.getText().isEmpty()) {
			config2.dialog(Alert.AlertType.ERROR, "Zip Not Empty");
			txtZip.requestFocus();
		} else {
			LibraryMember member = new LibraryMember();
			member.setAddress(
					new Address(txtStreet.getText(), txtCity.getText(), txtState.getText(), txtZip.getText()));
			member.setFirstName(txtFirstName.getText());
			member.setLastName(txtLastName.getText());
			member.setPhoneNumber(txtPhone.getText());
			member.setMemberId(Long.valueOf(txtMemberId.getText()));
			LibraryMemberImpl libraryMemberImpl = new LibraryMemberImpl();
			libraryMemberImpl.add(member);
			List<LibraryMember> members = libraryMemberImpl.getAllItems();
			tableData.setItems(FXCollections.observableArrayList(members));

			clear();
			// selectData();
			// auto();
			config2.dialog(Alert.AlertType.INFORMATION, "Success Save Data. . .");
		}
	}

	@FXML
	private void aksiBack(ActionEvent event) {
		paneCrud.setOpacity(0);
		new FadeInUpTransition(paneTabel).play();
	}
}
